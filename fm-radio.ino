#include <LiquidCrystal.h>
#include <SPI.h>

#include <Wire.h>

uint16_t channel = 187;

#define BOOT_CONFIG_LEN 12
char boot_config[] = {
  /* register 0x02 */
  0b11000001,
    /* 
     * DHIZ audio output high-z disable
     * 1 = normal operation
     *
     * DMUTE mute disable 
     * 1 = normal operation
     *
     * MONO mono select
     * 0 = stereo
     *
     * BASS bass boost
     * 0 = disabled
     *
     * RCLK NON-CALIBRATE MODE 
     * 0 = RCLK is always supplied
     *
     * RCLK DIRECT INPUT MODE 
     * 0 = ??? not certain what this does
     *
     * SEEKUP
     * 0 = seek in down direction
     *
     * SEEK
     * 0 = disable / stop seek (i.e. don't seek)
     */
  0b00000011,
    /* 
     * SKMODE seek mode: 
     * 0 = wrap at upper or lower band limit and contiue seeking
     *
     * CLK_MODE clock mode
     *  000 = 32.768kHZ clock rate (match the watch cystal on the module) 
     *
     * RDS_EN radio data system enable
     * 0 = disable radio data system
     *
     * NEW_METHOD use new demodulate method for improved sensitivity
     * 0 = presumably disabled 
     *
     * SOFT_RESET
     * 1 = perform a reset
     *
     * ENABLE power up enable: 
     * 1 = enabled
     */ 
  /* register 0x03 */
    /* Don't bother to tune to a channel at this stage*/
  0b00000000, 
    /* 
     * CHAN channel select 8 most significant bits of 10 in total
     * 0000 0000 = don't boher to program a channel at this time
     */
  0b00000000,
    /* 
     * CHAN two least significant bits of 10 in total 
     * 00 = don't bother to program a channel at this time
     *
     * DIRECT MODE used only when test
     * 0 = presumably disabled
     *
     * TUNE commence tune operation 
     * 0 = disable (i.e. don't tune to selected channel)
     *
     * BAND band select
     * 00 = select the 87-108MHz band
     *
     * SPACE channel spacing
     * 00 = select spacing of 100kHz between channels
     */    
  /* register 0x04 */
  0b00001010, 
    /* 
     * RESERVED 15
     * 0
     *
     * PRESUMABLY RESERVED 14
     * 0
     *
     * RESERVED 13:12
     * 00
     *
     * DE de-emphasis: 
     * 1 = 50us de-emphasis as used in Australia
     *
     * RESERVED
     * 0
     *
     * SOFTMUTE_EN
     * 1 = soft mute enabled
     *
     * AFCD AFC disable
     * 0 = AFC enabled
     */
  0b00000000, 
    /* 
     *  Bits 7-0 are not specified, so assume all 0's
     * 0000 0000
     */  
  /* register 0x05 */
  0b10001000, 
    /* 
     * INT_MODE
     * 1 = interrupt last until read reg 0x0C
     *
     * RESERVED 14:12 
     * 000
     *
     * SEEKTH seek signal to noise ratio threshold
     * 1000 = suggested default
     */   
  0b00001111, 
    /* 
     * PRESUMABLY RESERVED 7:6
     * 00
     *
     * RESERVED 5:4
     * 00
     *
     * VOLUME
     * 1111 = loudest volume
     */ 
  /* register 0x06 */
  0b00000000, 
    /* 
     * RESERVED 15
     * 0
     *
     * OPEN_MODE open reserved registers mode
     * 00 = suggested default
     *
     * Bits 12:8 are not specified, so assume all 0's
     * 00000
     */   
  0b00000000, 
    /* 
     *  Bits 7:0 are not specified, so assume all 0's
     *  00000000
     */    
  /* register 0x07 */
  0b01000010, 
    /* 
     *  RESERVED 15 
     * 0
     *
     * TH_SOFRBLEND threshhold for noise soft blend setting
     * 10000 = using default value
     *
     * 65M_50M MODE 
     * 1 = only applies to BAND setting of 0b11, so could probably use 0 here too
     *
     * RESERVED 8
     * 0
     */      
  0b00000010, 
    /*   
     *  SEEK_TH_OLD seek threshold for old seek mode
     * 000000
     *
     * SOFTBLEND_EN soft blend enable
     * 1 = using default value
     *
     * FREQ_MODE
     * 0 = using defualt value
     */  
};

#define TUNE_CONFIG_LEN 4
char tune_config[] = {
  /* register 0x02 */
  0b11000000, 
    /* 
     * DHIZ audio output high-z disable
     * 1 = normal operation
     *
     * DMUTE mute disable 
     * 1 = normal operation
     *
     * MONO mono select
     * 0 = mono
     *
     * BASS bass boost
     * 0 = disabled
     *
     * RCLK NON-CALIBRATE MODE 
     * 0 = RCLK is always supplied
     *
     * RCLK DIRECT INPUT MODE 
     * 0 = ??? not certain what this does
     *
     * SEEKUP
     * 0 = seek in down direction
     *
     * SEEK
     * 0 = disable / stop seek (i.e. don't seek)
     */   
   0b00000001, 
    /* 
     * SKMODE seek mode: 
     * 0 = wrap at upper or lower band limit and contiue seeking
     *
     * CLK_MODE clock mode
     * 000 = 32.768kHZ clock rate (match the watch cystal on the module) 
     *
     * RDS_EN radio data system enable
     * 0 = disable radio data system
     *
     * NEW_METHOD use new demodulate method for improved sensitivity
     * 0 = presumably disabled 
     *
     * SOFT_RESET
     * 0 = don't reset this time around
     *
     * ENABLE power up enable: 
     * 1 = enabled
     */ 
   /* register 0x03 */
   /* Here's where we set the frequency we want to tune to */
   (channel >> 2), 
    /* CHAN channel select 8 most significant bits of 10 in total   */
   ((channel & 0b11) << 6 ) | 0b00010000
    /* 
     *  CHAN two least significant bits of 10 in total 
     *
     * DIRECT MODE used only when test
     * 0 = presumably disabled
     *
     * TUNE commence tune operation 
     * 1 = enable (i.e. tune to selected channel)
     *
     * BAND band select
     * 00 = select the 87-108MHz band
     *
     * SPACE channel spacing
     * 00 = select spacing of 100kHz between channels
     */  
};

#define RDA5807M_ADDRESS 0x10 

#define ROTARY_LEFT 8
#define ROTARY_RIGHT 9
#define BUTTON A2

#define LCDLEN 16

#define PIN_MOSI 11
#define PIN_MISO 12
#define PIN_SCK 13
#define PIN_SS 10


// Create an LCD object. Parameters: (RS, E, D4, D5, D6, D7):
LiquidCrystal lcd = LiquidCrystal(2, 3, 4, 5, 6, 7);

void displayThing(byte status, char messages[][LCDLEN], int line) {
  if (!status) return;
  lcd.setCursor(0, line);
  lcd.print("                ");
  lcd.setCursor(0, line);
  lcd.print(messages[status - 1]);
}

enum {unchanged=0, negative=1, positive=2};

// This function is like a more generic version of a "Posedge" function I wrote
byte stateful (byte current, byte *last) { // Changed from static byte to a pointer so I can reuse the code with a pushbutton
  int diff = (current - *last) % 3;
  *last = current;
  return diff ? (diff < 0)
                 ? negative 
                 : positive
              : unchanged;
}

byte rotaryEncoder() {
  static byte last = 0; // Kept here due to code reusability
  const byte graycode[] = {0b00, 0b01, 0b11, 0b10}; // Use this to convert regular int to graycode & vice versa
  // Turn our rotary digital pin values into a graycode, map the graycode to an int, then check the diffs & return
  // an enum status corresponding to if we have ticked in a direction.
  return stateful(graycode[  (!digitalRead(ROTARY_RIGHT) << 1)  
                           | (!digitalRead(ROTARY_LEFT) << 0)],
                  &last);
}

byte button() {
  static byte last = 0;
  return stateful(digitalRead(BUTTON), //The button has a pullup resistor, press to ground.
                  &last);
}

int clip(int x, int low, int high) {
  return (x > high)? high:
         (x < low)? low:
         x;  
}

int potWrite(int value) {
  const int address = B00000000;
  digitalWrite(PIN_SS, LOW);

  SPI.transfer(address);
  SPI.transfer(value);
  delay(30);

  digitalWrite(PIN_SS, HIGH);
  return value; 
}

int potRead() {
  const int address = B00001100;
  digitalWrite(PIN_SS, LOW);

  SPI.transfer(address);
  int data = SPI.transfer(B00000000);
  delay(30);

  digitalWrite(PIN_SS, HIGH);
  return data;
}

int percentToByte(int p) {
  return p * 255.0 / 100;
}

int byteToPercent(int b) {
  return b * 100.0 / 255;
}

void clearLine(int line) {
  lcd.setCursor(0, line);
  lcd.print("                ");
  lcd.setCursor(0, line);
}

#include <stdarg.h>

void lcdprintf(int lineNum, char fmtstr[LCDLEN], ...) {
  char line[LCDLEN] = {0};
  va_list args;
  va_start(args, fmtstr);
  clearLine(lineNum);
  vsnprintf(line, LCDLEN, fmtstr, args);
  lcd.print(line);
  va_end(args);
}

#define VOL_INCR 10
#define CHANNEL_INCR 1
#define CHANNEL_FREQ_OFFSET 870
#define FREQ_SCALE 10 

void displayVolume(int volume) {
  lcdprintf(0, "Volume: %d%%", volume);
}

void displayFrequency(int freq) {
  lcdprintf(0, "Freq: %d.%d", freq / FREQ_SCALE, freq % FREQ_SCALE);
}

#define VOL_SCALE 6

int setVolume(int volume) {
  volume = clip(volume, 0, 100);
  potWrite(percentToByte(volume) / VOL_SCALE);
  displayVolume(volume);
  return volume;  
}

void displayChannel(int channel) {
  displayFrequency(channel + CHANNEL_FREQ_OFFSET);  
}

int setChannel(int channel) {
  tune_config[2] = (channel >> 2); 
  tune_config[3] = ((channel & 0b11) << 6 ) | 0b00010000;
  Wire.begin();
  Wire.beginTransmission(RDA5807M_ADDRESS);
  Wire.write(tune_config, TUNE_CONFIG_LEN);
  Wire.endTransmission();
  displayChannel(channel);
  return channel;
}

void configureDemod() {
  Wire.begin();
  Wire.beginTransmission(RDA5807M_ADDRESS);
  Wire.write(boot_config, BOOT_CONFIG_LEN);
  Wire.endTransmission();
  Wire.beginTransmission(RDA5807M_ADDRESS);
  Wire.write(tune_config, TUNE_CONFIG_LEN);
  Wire.endTransmission();  
}

void configureRotary() {
  pinMode(ROTARY_LEFT, INPUT);
  pinMode(ROTARY_RIGHT, INPUT);
  pinMode(BUTTON, INPUT);  
}

void configureVolumeControl() {
  pinMode(PIN_SS, OUTPUT);
  SPI.begin();
}
void configureDisplay() {
  lcd.begin(LCDLEN, 2);
  lcd.setCursor(0, 0);  
}

void setup() {
  
  Serial.begin(9600);

  enum {volume_mode, frequency_mode};
  int radio_mode = volume_mode;
  byte rotary_status = rotaryEncoder();

  configureDisplay();
  configureRotary();
  configureVolumeControl();
  configureDemod();
  
  int volume = setVolume(10);
  
  displayVolume(volume);
  
  for (;;) {
    if (button() == positive) {
      radio_mode ^= 1;
      if (radio_mode == volume_mode)
        displayVolume(volume);
      else
        displayChannel(channel); 
    }
    
    if ((rotary_status = rotaryEncoder()) != unchanged) {
      if (radio_mode == volume_mode)
        volume = setVolume(volume + ((rotary_status == positive)? VOL_INCR : -VOL_INCR));    
      else
        channel = setChannel(channel + ((rotary_status == positive)? CHANNEL_INCR : -CHANNEL_INCR));
    } 
  }
}

void loop() { /* nope */ }
